import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/services/sqflite_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetch_history_login() async {
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool? isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if(isLoggedIn == null){
      emit(AuthBlocLoginState());
    }else{
      if(isLoggedIn){
        emit(AuthBlocLoggedInState());
      }else{
        emit(AuthBlocLoginState());
      }
    }
  }

  Future<void> login_user(User user) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    print(user.email);
    print(user.password);

    try {
      emit(AuthBlocLoadingState());

      SqfliteService sqfliteService = new SqfliteService();

      await sqfliteService.select(user).then((value) async {

        if (value.length > 0) {
          await sharedPreferences.setBool("is_logged_in", true);
          String data = user.toJson().toString();
          sharedPreferences.setString("user_value", data);
          emit(AuthBlocLoggedInState());
        } else {
          emit(AuthBlocErrorState("Login gagal , periksa kembali inputan anda"));
        }

      });
    } catch(e) {
      print(e.toString());
      emit(AuthBlocErrorState("Login gagal , periksa kembali inputan anda"));
    }
  }
}
