import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/services/sqflite_service.dart';

part 'register_bloc_state.dart';

class RegisterBlocCubit extends Cubit<RegisterBlocState> {
  RegisterBlocCubit() : super(RegisterBlocInitialState());

  Future<void> register_user(User user) async {
    SqfliteService sqfliteService = new SqfliteService();

    print(user.email);
    print(user.password);
    print(user.userName);

    emit(RegisterBlocLoadingState());
    try {
      await sqfliteService.insert(user);
      emit(RegisterBlocSuccesState());
    } catch(e) {
      print(e.toString());
      emit(RegisterBlocErrorState("Email sudah terdaftar"));
    }
  }
}