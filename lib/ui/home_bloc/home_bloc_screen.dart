import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import '../../bloc/home_bloc/home_bloc_cubit.dart';
import 'home_bloc_loaded_screen.dart';
import '../extra/loading.dart';
import '../extra/error_screen.dart';

class HomeBlocScreen extends StatefulWidget {
  const HomeBlocScreen({Key? key}) : super(key: key);

  @override
  _HomeBlocScreenState createState() => _HomeBlocScreenState();
}

class _HomeBlocScreenState extends State<HomeBlocScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBlocCubit, HomeBlocState>(builder: (context, state) {
      if (state is HomeBlocLoadedState) {
        return HomeBlocLoadedScreen(data: state.data);
      }

      if (state is HomeBlocNotConnectedState) {
        return Scaffold(
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.wifi_off,
                  size: 50,
                ),
                Text("Tidak Ada Koneksi Internet"),
                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 100),
                  child: CustomButton(
                    text: 'Refresh',
                    onPressed: checkConnection,
                    height: 100,
                  ),
                ),
              ],
            ),
          ),
        );
      }

      if (state is HomeBlocErrorState) {
        return ErrorScreen(message: state.error);
      }

      if (state is HomeBlocInitialState) {
        return Scaffold();
      }

      if (state is HomeBlocLoadingState) {
        return LoadingIndicator();
      }

      return LoadingIndicator();
    });
  }

  void checkConnection() {
    final homeBloc = BlocProvider.of<HomeBlocCubit>(context);
    homeBloc.checkConnection();
  }
}
