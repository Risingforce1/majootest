import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:majootestcase/models/movie_response.dart';

class HomeDetailMovie extends StatefulWidget {
  final Data? data;

  const HomeDetailMovie({Key? key, this.data}) : super(key: key);

  @override
  _HomeDetailMovieState createState() => _HomeDetailMovieState();
}

class _HomeDetailMovieState extends State<HomeDetailMovie> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: CustomScrollView(
        physics: const BouncingScrollPhysics(
          parent: AlwaysScrollableScrollPhysics(),
        ),
        slivers: [
          SliverAppBar(
            stretch: true,
            onStretchTrigger: () {
              return Future<void>.value();
            },
            expandedHeight: 300.0,
            flexibleSpace: FlexibleSpaceBar(
              stretchModes: const <StretchMode>[
                StretchMode.zoomBackground,
                StretchMode.blurBackground,
                StretchMode.fadeTitle,
              ],
              centerTitle: true,
              title: Text(
                '${widget.data!.l} - ${widget.data!.year}',
                style: TextStyle(
                  fontSize: 14,
                ),
              ),
              background: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  Hero(
                    tag: "${widget.data!.i!.imageUrl}",
                    child: Image.network(
                      '${widget.data!.i!.imageUrl}',
                      fit: BoxFit.cover,
                    ),
                  ),
                  const DecoratedBox(
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment(0.0, 0.5),
                        end: Alignment.center,
                        colors: <Color>[
                          Color(0x60000000),
                          Color(0x00000000),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: SizedBox(
              height: 10,
            ),
          ),
          SliverGrid(
            gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 200.0,
            ),
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Column(
                    children: [
                      Image.network(
                        widget.data!.series![index].i!.imageUrl!,
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 10, left: 5, right: 5),
                        child: Text(
                          widget.data!.series![index].l!,
                          textDirection: TextDirection.ltr,
                          style: TextStyle(
                              fontSize: 11,
                              fontWeight: FontWeight.bold,
                              color: Colors.grey
                          ),
                        ),
                      )
                    ],
                  ),
                );
              },
              childCount: widget.data!.series != null ? widget.data!.series!.length : 0,
            ),
          )
        ],
      ),
    );
  }
}
