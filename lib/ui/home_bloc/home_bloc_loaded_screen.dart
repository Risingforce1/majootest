import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:majootestcase/models/movie.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/ui/home_bloc/home_detail_movie.dart';

class HomeBlocLoadedScreen extends StatefulWidget {
  final List<Data>? data;

  const HomeBlocLoadedScreen({Key? key, this.data}) : super(key: key);

  @override
  _HomeBlocLoadedScreenState createState() => _HomeBlocLoadedScreenState();
}

class _HomeBlocLoadedScreenState extends State<HomeBlocLoadedScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: Text("Movie"),
      ),
      body: MasonryGridView.count(
        itemCount: widget.data!.length,
        crossAxisCount: 2,
        mainAxisSpacing: 4,
        crossAxisSpacing: 4,
        itemBuilder: (context, index) {
          return movieItemWidget(widget.data![index]);
        },
      ),
    );
  }

  Widget movieItemWidget(Data data) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => HomeDetailMovie(
                data: data,
              ),
            ),
          );
        },
        child: Container(
          color: Colors.grey.shade900,
          child: Column(
            children: [
              Hero(
                tag: "${data.i!.imageUrl}",
                child: Image.network(
                  data.i!.imageUrl!,
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                child: Text(
                  data.l!,
                  textDirection: TextDirection.ltr,
                  style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                      color: Colors.grey),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
