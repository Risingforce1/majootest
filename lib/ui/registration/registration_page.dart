import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/register_bloc/register_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';

class RegistrationPage extends StatefulWidget {
  const RegistrationPage({Key? key}) : super(key: key);

  @override
  _RegistrationPageState createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {

  final _usernameController = TextController();
  final _emailController = TextController();
  final _passwordController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: Text("Register"),
      ),
      body: BlocListener<RegisterBlocCubit, RegisterBlocState>(
        listener: (context, state) {
          print("Listen");
          print(state);
          
          if (state is RegisterBlocLoadingState) {
            print("Loding");
          }

          if (state is RegisterBlocErrorState) {
            Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text("${state.error}"),
                  backgroundColor: Colors.red,
                )
            );
          }

          if (state is RegisterBlocSuccesState) {
            Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text("Register Berhasil!"),
                  backgroundColor: Colors.green,
                )
            );
            
            Future.delayed(Duration(seconds: 2), () {
              Navigator.pop(context);
            });
          }
          
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(top: 15, left: 15, bottom: 15, right: 15),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Silahkan melakukan registrasi untuk pengguna baru.',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    // color: colorBlue,
                  ),
                ),
                SizedBox(
                  height: 9,
                ),
                _form(),
                SizedBox(
                  height: 50,
                ),
                ElevatedButton(
                  onPressed: handleSimpan,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: Center(
                      child: Text("Register"),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            label: 'Username',
            hint: 'example data',
            controller: _usernameController,
          ),
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null)
                return pattern.hasMatch(val) ? null : 'Masukkan e-mail yang valid';
              return '';
            },
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  void handleSimpan() async {
    final String? _username = _usernameController.value;
    final String? _email = _emailController.value;
    final String? _password = _passwordController.value;

    if (formKey.currentState?.validate() == true &&
        _email != null &&
        _password != null &&
        _username != null
    ) {
      final registerBloc = BlocProvider.of<RegisterBlocCubit>(context);
      User user = new User(
        userName: _username,
        email: _email,
        password: _password
      );

      await registerBloc.register_user(user);
    }
  }
}
