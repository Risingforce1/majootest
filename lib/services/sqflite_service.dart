import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/services/sqflite_access.dart';
import 'package:sqflite/sqflite.dart';

class SqfliteService {
  static const userTable = 'tb_user';
  static const id = 'id';
  static const username = 'username';
  static const email = 'email';
  static const password = 'password';
  static const createdBy = 'created_by';

  SqfliteAccess _dbHelper = new SqfliteAccess();

  Future<int> insert(User user) async {
    print("Save to DB");
    Database db = await _dbHelper.initDB();
    final sql = '''
      INSERT INTO ${SqfliteService.userTable} (
        ${SqfliteService.username},
        ${SqfliteService.email},
        ${SqfliteService.password},
        ${SqfliteService.createdBy}
      ) VALUES (
        ?, ?, ?, ?
      )
    ''';

    List<dynamic> params = [
      user.userName,
      user.email,
      user.password,
      user.userName
    ];
    final result = await db.rawInsert(sql, params);
    print(result);
    return result;
  }

  Future<List> select(User user) async {
    print("Select from DB");
    Database db = await _dbHelper.initDB();

    final result = await db.query(SqfliteService.userTable,
        where: "${SqfliteService.email} = ? AND ${SqfliteService.password} = ?",
        whereArgs: [user.email, user.password],
        limit: 1);

    return result;
  }
}
