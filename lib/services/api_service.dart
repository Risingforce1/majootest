import 'package:dio/dio.dart';
import 'package:majootestcase/models/movie_response.dart';

class ApiServices {
  Future<MovieResponse?> getMovieList() async {
    try {
      var response = await Dio().get(
        "https://imdb8.p.rapidapi.com/auto-complete?q=game%20of%20thr",
        options: Options(
          sendTimeout: 12000,
          receiveTimeout: 12000,
          headers: {
            "x-rapidapi-key": "ab264c9dfbmsh64d9703fc8a3f97p1ec04djsnc030b1f015aa",
            "x-rapidapi-host": "imdb8.p.rapidapi.com"
          },
        ),
      );
      MovieResponse movieResponse = MovieResponse.fromJson(response.data);
      return movieResponse;
    } catch (e) {
      print("Error");
      print(e.toString());
      return null;
    }
  }
}
