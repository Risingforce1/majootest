import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class SqfliteAccess {
  Future<Database> initDB() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path + 'user.db';

    var userDatabase = openDatabase(path, version: 1, onCreate: _createDB,);
    return userDatabase;
  }

  void _createDB(Database db, int version) async {
    await db.execute('''
      CREATE TABLE tb_user (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        username TEXT,
        email TEXT UNIQUE,
        password TEXT,
        created_by TEXT
      )
    ''');
  }

}