import 'dart:async';
import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

Dio? dioInstance;

void createInstance() async {
  var options = BaseOptions(
      baseUrl: "https://imdb8.p.rapidapi.com/auto-complete?q=game%20of%20thr",
      connectTimeout: 12000,
      receiveTimeout: 12000,
      headers: {
        "x-rapidapi-key": "ab264c9dfbmsh64d9703fc8a3f97p1ec04djsnc030b1f015aa",
        "x-rapidapi-host": "imdb8.p.rapidapi.com"
      });

  dioInstance = new Dio(options);
  dioInstance!.interceptors.add(PrettyDioLogger(
      requestHeader: true,
      requestBody: true,
      responseBody: true,
      responseHeader: false,
      error: true,
      compact: true,
      maxWidth: 90));
}

Future<Dio?> dio() async {
  dioInstance?.options.baseUrl = "https://imdb8.p.rapidapi.com/auto-complete?q=game%20of%20thr";
  return dioInstance;
}